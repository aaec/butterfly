class Console:
    """
    A class to handle user input and output.

    This purpose of this class is to decouple the user input / output from the main logic.
    This will make dealing with color / no color terms, terminal widths, and consistent printing styles easier.
    """

    # ANSI escape codes
    colors = {
        'magenta': '\033[35m',
        'bold_green': '\033[1;32m',
        'bold_red': '\033[1;310m',
        'bold_blue': '\033[1;34m',
        'bold_yellow': '\033[1;33m',
        'on_red': '\033[41;37m',
        'normal': '\033[0;21;24;27m',
    }
    # the bit inside the brackets in butterfly output
    prefixes = {
        'success': f'{colors["bold_green"]}+{colors["normal"]}',
        'failure': f'{colors["bold_red"]}-{colors["normal"]}',
        'debug': f'{colors["bold_red"]}DEBUG{colors["normal"]}',
        'info': f'{colors["bold_blue"]}*{colors["normal"]}',
        'warning': f'{colors["bold_yellow"]}!{colors["normal"]}',
        'error': f'{colors["on_red"]}ERROR{colors["normal"]}',
        'exception': f'{colors["on_red"]}ERROR{colors["normal"]}',
        'critical': f'{colors["on_red"]}CRITICAL{colors["normal"]}',
        'info_once': f'{colors["bold_blue"]}*{colors["normal"]}',
        'warning_once': f'{colors["bold_yellow"]}!{colors["normal"]}',
        'question': f'{colors["magenta"]}?{colors["normal"]}'
    }
    prefixes_no_color = {
        'success': '+',
        'failure': '-',
        'debug': 'DEBUG',
        'info': '*',
        'warning': '!',
        'error': 'ERROR',
        'exception': 'ERROR',
        'critical': 'CRITICAL',
        'info_once': '*',
        'warning_once': '!',
        'question': '?'
    }

    # what types of messages get printed for each verbosity level. To add another level, add it to this dict and make
    # a command line option to enable it
    VERBOSITY_MESSAGES = {
        'quiet': ['success', 'failure', 'error', 'critical'],
        'normal': ['success', 'failure', 'info', 'warning', 'error', 'exception', 'critical'],
        'debug': ['success', 'failure', 'info', 'warning', 'error', 'exception', 'critical', 'debug'],
    }

    def __init__(self, color=True, verbosity='normal'):
        # determines weather my output contains ANSI escape sequences
        # TODO: test if terminal supports color. If our test conflicts with what the the constructors `color` param is
        # then send a warning to user
        self.__info_once_msgs = set()
        self.__warn_once_msgs = set()
        self.__color_term = color
        self.prefixes = self.prefixes_no_color if not self.__color_term else self.prefixes
        # three levels of verbosity: quiet
        self.verbosity = verbosity

    def print_card(self):
        r"""
        Prints the name butterfly to the console in some cool word art or ascii art
        """
        print(f'~ ~ ~  {self.colors["magenta"]}B U T T E R F L Y{self.colors["normal"]}  ~ ~ ~')

    def print_info(self, msg):
        """
        print an info message to the console

        Arguments:
            msg (str) : message to print

        Examples:
            >>> print_info("Never Trust a Cop")

            [*] Never Trust a Cop
        """
        if 'info' in self.VERBOSITY_MESSAGES[self.verbosity]:
            print(f'[{self.prefixes["info"]}] {msg}')

    def print_debug(self, msg):
        """
        print debugging info

        Arguments:
            msg (str) : message to print

        Examples:
            >>> print_debug("That variable is being weird 👀")

            [DEBUG] That variable is being weird 👀
        """
        if 'debug' in self.VERBOSITY_MESSAGES[self.verbosity]:
            print(f'[{self.prefixes["debug"]}] {msg}')

    def print_error(self, msg):
        """
        print type for when something goes wrong

        Arguments:
            msg (str) : message to print

        Examples:
            >>> print_error("502")

            [ERROR] 502
        """
        if 'error' in self.VERBOSITY_MESSAGES[self.verbosity]:
            print(f'[{self.prefixes["error"]}] {msg}')

    def print_critical(self, msg):
        """
        print type for when something goes so horribly wrong that butterfly crashes. this should be the last thing your
        code calls before calling exit()

        Arguments:
            msg (str) : message to print

        Examples:
            >>> print_error("Seg fault")

            [CRITICAL] Seg Fault
            exiting.
        """
        if 'critical' in self.VERBOSITY_MESSAGES[self.verbosity]:
            print(f'[{self.prefixes["critical"]}] {msg}\nexiting.')

    def print_success(self, msg):
        """
        print type for when an operation succeeds

        Arguments:
            msg (str) : message to print

        Examples:
            >>> print_success("Pushed to branch master")

            [+] Pushed to branch master
        """
        if 'success' in self.VERBOSITY_MESSAGES[self.verbosity]:
            print(f'[{self.prefixes["success"]}] {msg}')

    def print_failure(self, msg):
        """
        print type for when an operation fails

        Arguments:
            msg (str) : message to print

        Examples:
            >>> print_failure("Failed to push to branch master")

            [-] Failed to push to branch master
        """
        if 'failure' in self.VERBOSITY_MESSAGES[self.verbosity]:
            print(f'[{self.prefixes["failure"]}] {msg}')

    def print_warning(self, msg):
        """
        print type for when you want to warn the user about something that is happening

        Arguments:
            msg (str) : message to print

        Examples:
            >>> print_warning("Core temperatures reaching threshold")

            [!] Core temperatures reaching threshold
        """
        if 'warning' in self.VERBOSITY_MESSAGES[self.verbosity]:
            print(f'[{self.prefixes["warning"]}] {msg}')

    def print_exception(self, msg):
        """
        print type to wrap your exceptions in

        Arguments:
            msg (str) : message to print

        Examples:
            >>> try:
            ...    result = my_dict[value]
            ... except KeyError:
            ...     print_exception("KeyError: value not in my_dict")

            [!] KeyError: value not in my_dict
       """
        if 'exception' in self.VERBOSITY_MESSAGES[self.verbosity]:
            print(f'[{self.prefixes["exception"]}] {msg}')

    def print_info_once(self, msg):
        """
        print an info message once and only once. If this Console object has already printed this msg using
        print_info_once than this method will not print anything. Messages printed using another print method
        don't count towards this limit.

        Arguments:
            msg (str) : message to print

        Examples:
            >>> print_info_once("message")
            >>> print_info_once("message")
            >>> print_info("message")

            [*] message
            [*] message
       """
        if 'info' in self.VERBOSITY_MESSAGES[self.verbosity] and msg not in self.__info_once_msgs:
            self.__info_once_msgs.add(msg)
            print(f'[{self.prefixes["info"]}] {msg}')

    def print_warning_once(self, msg):
        """
        print an warning message once and only once. If this Console object has already printed this msg using
        print_warning_once than this method will not print anything. Messages printed using another print method
        don't count towards this limit.

        Arguments:
            msg (str) : message to print

        Examples:
            >>> print_warning_once("watch out")
            >>> print_warning_once("watch out")
            >>> print_warning("watch out")

            [*] watch out
            [*] watch out
       """
        if 'warning' in self.VERBOSITY_MESSAGES[self.verbosity] and msg not in self.__warn_once_msgs:
            self.__warn_once_msgs.add(msg)
            print(f'[{self.prefixes["warning"]}] {msg}')


    # TODO
    def password_prompt(self, msg, verify=False):
        """
         prompts the user for a password, hiding the characters as they type

         Arguments:
           prompt (str): The prompt to show
           verify (bool): Whether to prompt the user second time to confirm the password

         Returns:
           The password provided

         Examples:
             >>> password_prompt("set your password:", verify=True)

                set your password:
                again:
         """

    # TODO
    def options(self, prompt, opts, default=None):
        """
        Presents the user with a prompt (typically in the
        form of a question) and a number of options.

        Arguments:
          prompt (str): The prompt to show
          opts (list): The options to show to the user
          default: The default option to choose

        Returns:
          The users choice in the form of an integer.

        Examples:
            >>> options("Select a color", ("red", "green", "blue"), "green")

             [?] select a color A
                   1) red
                   2) green
                   3) blue
                 Choice:
        """

    # TODO
    def yes_no(self, prompt, default=None):
        """
        Presents the user with prompt (typically in the form of question)
        which the user must answer yes or no.

        Arguments:
          prompt (str): The prompt to show
          default: The default option;  `True` means "yes"

        Returns:
          `True` if the answer was "yes", `False` if "no"

        Examples:
            >>> yes_no("is it good?")

             [?] is it good? [yes/no]
        """

    def text_prompt(self, prompt, default=None):
        """
        Presents the user with prompt (typically in the form of question)
        which the user must answer with a string.

        Arguments:
          prompt (str): The prompt to show
          default: The default option

        Returns:
          users answer

        Examples:
            >>> text_prompt("Do you like waffles?")

             [?] Do you like waffles?
                 Answer:
        """
        print(f'[{self.prefixes["question"]}] {prompt}')
        return input(f'    Answer:')

    # TODO
    def pause(self, n=None):
        """
        Waits for either user input or a specific number of seconds.

        Examples:
            >>> pause(3)

            [x] Waiting
            [x] Waiting: 1 ...
            [x] Waiting: 1, 2 ...
            [x] Waiting: 1, 2, 3 ...
            [+] Waiting: Done
        """
