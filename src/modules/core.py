import os
import sys

from tinydb import TinyDB, Query
from jsonschema import validate
import tinydb_encrypted_jsonstorage as tae
import hashlib
import json

ENDPOINT_SCHEMA_PATH = "./src/Endpoint.json"
DB_PATH = "./.encrypted_db"


def load_db(console, options):
    """
     load the database located at DB_PATH, prompting for database password

     Arguments:
       console (Console): The Console to display messages to
       options (dict): The options supplied from the cli

     Returns:
       a TinyDB object
     """
    console.print_debug("initializing db")
    master_pass = console.text_prompt("Database password?")
    master_pass_hash = hashlib.sha512(master_pass.encode('UTF-8')).hexdigest()
    confirmation_hash = hashlib.sha512(console.text_prompt("again:").encode('UTF-8')).hexdigest()
    if master_pass_hash != confirmation_hash:
        console.print_error("Passwords do not match")
        sys.exit('Password do not match')
    try:
        db = TinyDB(encryption_key=master_pass, path=DB_PATH, storage=tae.EncryptedJSONStorage)
        # check that password hash is the same, sort of like magic number for the database
        # it's the only way I can think to verify that the database was actually successfully
        # decoded
        query = Query()
        found_hash = db.table('meta').search(query.master_pass_hash.exists())[0]['master_pass_hash']
        assert (found_hash == master_pass_hash)
    except:
        console.print_critical('Wrong Password. Unable to open database')
        sys.exit(1)
    console.print_success(f'Database loaded')
    console.print_debug(f'Database has {len(db.table("endpoints").all())} endpoints, location {DB_PATH}')
    console.print_debug(db.table('endpoints').all())
    return db

def add_endpoint(console, db):
    """
    Action.
    Name: addEndpoint -- interactively add an endpoint
    Synopsis:
        butterfly [<options>...] addEndpoint
    Options:
     Arguments:
       console (Console): The Console to display messages to
       db (TinyDB): butterfly's database
    Description:
        gives the user an interactive prompt for adding an endpoint
    """


def _add_endpoint(console, db, endpoint):
    """
     add given endpoint to the database, NOT the action, just an internal helper

     Arguments:
       console (Console): The Console to display messages to
       db (TinyDB): The database to add the endpoint to
       endpoint (Endpoint): The JSON endpoint to add
     Options:
     Arguments:
       console (Console): The Console to display messages to
     Returns:
       True on success, False on fail
     """
    # load schema file
    endpoint_schema = ''
    with open(ENDPOINT_SCHEMA_PATH, 'r') as f:
        endpoint_schema = json.load(f)
    console.print_debug(f'endpoint JSON schema loaded {ENDPOINT_SCHEMA_PATH}')
    try:
        validate(instance=endpoint, schema=endpoint_schema)
    except:
        console.print_error(f'unable to add endpoint, does not conform to schema {ENDPOINT_SCHEMA_PATH}')
        return False
    db.table('endpoints').insert(endpoint)
    return True


def setup(console):
    """
    Action.
    Name: setup -- interactive action to `initialize` and then `addEndpoint` in an interactive loop
    Synopsis:
        butterfly [<options>...] setup
    Options:
     Arguments:
       console (Console): The Console to display messages to
    Description:
        combines the functionality of `initialize` with `addEndpoint` for quick setup
    """
    # add serving machine's info to database as an endpoint
    db = initialize(console)
    # TODO: prompt the user to add the host that butterfly is on to the database
    while (True):
        add_endpoint(console, db)

def initialize(console):
    """
     Action.
     Name: initialize -- initialize butterfly db
     Synopsis:
        butterfly [<options>...] init
    Description:
        initializes the butterfly endpoint database, prompts user to set master database password
    Options:
     Arguments:
       console (Console): The Console to display messages to
     Returns:
        TinyDB object for the initialized db
     """
    console.print_debug('initializing')
    # get master password used to encrypt database
    # TODO: create Console.password_prompt
    master_pass = console.text_prompt("Master password for butterfly?")
    master_pass_hash = hashlib.sha512(master_pass.encode('UTF-8')).hexdigest()
    confirmation_hash = hashlib.sha512(console.text_prompt("again:").encode('UTF-8')).hexdigest()
    if master_pass_hash != confirmation_hash:
        console.print_error("Passwords do not match")
        sys.exit('paswords do not match')
    # get database
    # have to remove old db so that Tiny doesn't try to open it
    try:
        os.remove(DB_PATH)
    except:
        pass
    db = TinyDB(encryption_key=master_pass, path=DB_PATH, storage=tae.EncryptedJSONStorage)
    db_meta = db.table('meta')
    db_endpoints = db.table('endpoints')
    db_meta.insert({'master_pass_hash': master_pass_hash})
    return db

    # TODO
    def print_help():
        """
         Action.
         Name: help -- print documentation for a action
         Synopsis:
            butterfly help <action>
        Description:
            prints the docstring for the main method of the given action
        Options:

        Arguments:

         """
        # print the docstring for the given action
        # TODO
        pass
