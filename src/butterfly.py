#!/usr/bin/env python3

import modules.core
import modules.passwordChange
from console import Console
import sys
from optparse import OptionParser

DB_PATH = "./.encrypted_db"
VERSION = "0"


def parse_args():
    """
    Returns:
        `options, args` tuple, args[0] is the action
    """
    usage = "[!] Usage: butterfly.py [options] action [args]"
    parser = OptionParser(usage)

    # THIS IS WHERE YOU ADD NEW COMMAND LINE OPTIONS
    # documentation -> https://docs.python.org/2/library/optparse.html#defining-options
    parser.add_option('-v', '--verbose', action="store_const", dest="verbosity", const="debug")
    parser.add_option('-q', '--quiet', action="store_const", dest="verbosity", const="quiet")
    # Idea for a potential option:
    # parser.add_option('-i', '--interactive-on-fail', action='store_true', dest='interactive_on_fail')
    parser.add_option('--no-color', action='store_false', dest='color')
    parser.set_defaults(verbosity='normal', color='True', interactive_on_fail='False')
    parsed_options, parsed_args = parser.parse_args()
    if len(parsed_args) < 1:
        print(usage)
        sys.exit('usage error')
    return parsed_options, parsed_args


if __name__ == "__main__":
    options, args = parse_args()
    my_console = Console(verbosity=options.verbosity, color=options.color)
    my_console.print_card()
    if options.verbosity is "debug":
        my_console.print_debug("Debug mode enabled 😎")
    # switch flow to different actions based on cli arguments
    # Core module actions go here
    if args[0] == "init":
        modules.core.initialize(my_console)
        sys.exit(0)
    if args[0] == "help":
        modules.core.print_help(my_console, options, args)
        sys.exit(0)
    my_db = modules.core.load_db(my_console, options)

    # Put regular action modules here
    # if args[0] == "myAction":
    # call your action
    # sys.exit(0)
    if args[0] == "passwordChange":
        modules.passwordChange.password_change(my_console, my_db, options, args)
        sys.exit(0)
    my_console.print_error(f'Action not found: {args[0]}')